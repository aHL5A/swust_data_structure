/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
#include <list>
using namespace std;
int main()
{
	int n, m, t;
	list<int>p1, p2;
	cin>>n;
	while(n--){
		cin>>t;
		p1.push_back(t);
	}
	list<int>::iterator it = p1.begin();
	cin>>m;
	while(m--){
		int flag = 0;
		cin>>t;
		it = p1.begin();
		while(it != p1.end()){
			if(*it == t){
				it = p1.begin();
				flag = 1;
				break;
			}
			it++;
		}
		if(flag==0)p1.push_back(t);
	}
	it = p1.begin();
	while(it != p1.end()){
		cout<<*it<<" ";
		it++;
	}
	return 0;
}

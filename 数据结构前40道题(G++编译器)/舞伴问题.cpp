/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/116
 */

#include <iostream>
#include <queue>
using namespace std;
int main()
{
	queue<char>man;
	queue<char>woman;
	int m, n, t;
	char tmp;
	cin>>m;
	while(m--){
		cin>>tmp;
		man.push(tmp);
	}
	cin>>n;
	while(n--){
		cin>>tmp;
		woman.push(tmp);
	}
	cin>>t;
	while(--t){
		man.push(man.front());
		man.pop();
		woman.push(woman.front());
		woman.pop();
	}
	cout<<man.front()<<" "<<woman.front();
	return 0;
}

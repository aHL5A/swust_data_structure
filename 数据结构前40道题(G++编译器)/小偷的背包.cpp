/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
using namespace std;

int mmax(int a, int b)
{
	return a>b?a:b;
}

int main()
{
	int dp[200]={0}, s, n, w;
	cin>>s>>n;
	while(n--){
		cin>>w;
		for(int i = s; i >= 1; i--){
			if(dp[i-w]+w <= i && i-w >= 0)
				dp[i] = mmax(dp[i-1], dp[i-w]+w);
			if(dp[s]==s){
				cout<<"yes!";
				return 0;
			}
		}
	}
	cout<<"no!";
	return 0;
}

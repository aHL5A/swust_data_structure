/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
#include <vector>
using namespace std;
int main()
{
	int n;
	vector<int>p;
	cin>>n;
	if(n==0)cout<<0;
	while(n){
		p.push_back(n%2);
		n>>=1;
	}
	vector<int>::reverse_iterator it = p.rbegin();
	while(it != p.rend()){
		cout<<*it;
		it++;
	}
	return 0;
}

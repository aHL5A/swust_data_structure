/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int main()
{
	int n, m, t;
	vector<int>p;
	cin>>n;
	while(n--){
		cin>>t;
		p.push_back(t);
	}
	cin>>m;
	while(m--){
		cin>>t;
		p.push_back(t);
	}
	sort(p.begin(), p.end());
	for(vector<int>::iterator it = p.begin(); it != p.end(); it++){
		cout<<*it<<" ";
	}
	return 0;
}

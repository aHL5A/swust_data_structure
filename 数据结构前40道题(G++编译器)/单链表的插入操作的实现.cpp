/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
#include <list>
using namespace std;
int main()
{
	int n, t, i, data, len;
	list<int>p;
	cin>>n;
	len = n;
	while(n--){
		cin>>t;
		p.push_back(t);
	}
	cin>>i>>data;
	if(i < 1 || i > len){
		cout<<"error!";
		return 0;
	}
	list<int>::iterator pos = p.begin();
	for(int j = 1; j <= i-1; j++){
		pos++;
	}
	p.insert(pos, data);
	for(pos = p.begin(); pos != p.end(); pos++){
		cout<<*pos<<" ";
	}
	return 0;

}

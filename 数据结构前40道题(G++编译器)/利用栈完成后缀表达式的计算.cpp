/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
#include <stack>
#include <cctype>
#include <string>
#include <cstdio>
using namespace std;
int main()
{
	int i = 0, a, b;
	stack<int>p;
	string s;
	getline(cin,s);
	while(s[i] != '#'){
		if(isdigit(s[i])){
			p.push(int(s[i]-'0'));
			i++;
			continue;
		}
		if(s[i]!=' '){
			a = p.top();
			p.pop();
			b = p.top();
			p.pop();
		}
		if(s[i]=='+'){
			p.push(b+a);
		}
		if(s[i]=='-'){
			p.push(b-a);
		}
		if(s[i]=='*'){
			p.push(b*a);
		}
		if(s[i]=='/'){
			p.push(b/a);
		}
		i++;
	}
	cout<<p.top();
	return 0;
}

/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include<stdio.h>
int a[100][100];
int main()
{
	int i,j,n;
	scanf("%d",&n);
	a[1][1]=1;
	for(i=2;i<=n;i++)
	{
		for(j=1;j<=i;j++)
		{
			a[i][j]=a[i-1][j]+a[i-1][j-1];
		}
	}
	for(i=1;i<=n;i++)
	{
		for(j=1;j<=i;j++)
		printf("%d ",a[i][j]);
		printf("\n");
	}
	return 0;
}
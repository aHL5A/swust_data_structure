/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <stdio.h>
void left_num(int* a,int n,int m) {
    int out = 0,count = 0,i = 0;
    int *p = a;
    int num = 0;
    for(num = 0;num < n;num++) {
        *(a+num) = num+1;
    }
    while (out < n-1) {
        if (*(p+i) != 0) {
            count ++;
        }
        if (count == m) {
            count = 0;
            *(p+i) = 0;
            out++;
        }
        i++;
        if (i == n) {
            i = 0;
        }
    }
    for (num = 0; num < n; num++) {
        if (*(a+num) != 0) {
            printf("%d",*(a+num));
        }
    }
}

int main()
{
    int m,n;
    int a[50] = {0};
    scanf("%d %d",&n, &m);
    left_num(a,n,m);
    return 0;
}

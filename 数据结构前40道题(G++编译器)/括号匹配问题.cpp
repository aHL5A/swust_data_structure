/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
#include <stack>
#include <string>
using namespace std;
int main()
{
	stack<char>p;
	p.push('F');
	string s;
	cin>>s;
	for(int i = 0; i < s.length(); i++){
		if(s[i]=='(' || s[i] == '['){
			p.push(s[i]);
		}
		if(s[i]==')'){
			if(p.top()=='(')
				p.pop();
			else {
				cout<<"NO";
				return 0;
			}
		}
		if(s[i]==']'){
			if(p.top()=='[')
				p.pop();
			else {
				cout<<"NO";
				return 0;
			}
		}
	}
	if(p.top()=='F')cout<<"YES";
	else cout<<"NO";
	return 0;
}
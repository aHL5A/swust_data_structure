/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
#include <list>
using namespace std;
int main()
{
	bool flag = false;
	int n, t, item, data;
	list<int>p;
	cin>>n;
	while(n--){
		cin>>t;
		p.push_back(t);
	}
	cin>>item>>data;
	list<int>::iterator pos = p.begin();
	while(pos != p.end()){
		if(*pos == item){
			p.insert(pos, data);
			flag = true;
			break;
		}
		pos++;
	}
	if(!flag)p.push_back(data);
	for(pos = p.begin(); pos != p.end(); pos++){
		cout<<*pos<<" ";
	}
	return 0;
	
}

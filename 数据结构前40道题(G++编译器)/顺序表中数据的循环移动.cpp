/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
using namespace std;
int main()
{
	int n, p, ary[500];
	cin>>n;
	for(int i = 0; i < n; i++){
		cin>>ary[i];
	}
	cin>>p;
	if(p<=0 || p>=n){
		cout<<"error!";
		return 0;
	}
	for(int i = p, j = 0; j < n; j++){
		cout<<ary[i]<<" ";
		i = (++i)%n;
	}
	return 0;
}

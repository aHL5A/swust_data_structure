/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/15
 */

#include <iostream>
using namespace std;
int flag = 0;
typedef struct node{
	char data;
	node *L_child;
	node *R_child;
}Tree;

void Init(Tree *&T)
{
	char c;
	cin>>c;
	if(c=='#'&&flag==0){
		T = NULL;
		return;
	}
	if(c!='#'){
		flag=1;
		T = new Tree;
		T->data = c;
		Init(T->L_child);
		Init(T->R_child);
	}
	else T = NULL;
}

void Node2(Tree *T, int &all)
{
	if(T==NULL)return;
	if(T->L_child!=NULL && T->R_child!=NULL){
		all += 1;
	}
	if(T->L_child)Node2(T->L_child, all);
	if(T->R_child)Node2(T->R_child, all);
	return;
}

int main()
{
	Tree *T;
	Init(T);
	int all = 0;
	Node2(T, all);
	cout<<all;
	return 0;
}


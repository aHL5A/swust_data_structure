/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int main()
{
	int n, t, m;
	vector<int>p;
	cin>>n;
	m = n;
	while(n--){
		cin>>t;
		p.push_back(t);
	}
	sort(p.begin(), p.end());
	int len = 1;
	vector<int>::iterator it = ++p.begin();
	while(it != p.end()){
		if(*it != *(it-1)){
			if(len > m/2){
				cout<<*(it-1);
				return 0;
			}
			len = 1;
		}
		else len++;
		it++;
	}
	cout<<-1;
	return 0;
}

/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
#include <list>
#include <cctype>
#include <string>
#include <cstdio>
using namespace std;
int main()
{
	int i = 0;
	string s;
	list<char>num, alpha, other;
	cin>>s;
	while(i != s.length()){
		if(isalpha(s[i]))alpha.push_back(s[i]);
		else if(isdigit(s[i]))num.push_back(s[i]);
		else other.push_back(s[i]);
		i++;
	}
	list<char>::iterator it = num.begin();
	while(it != num.end()){
		printf("%c", *it);
		it++;
	}
	cout<<endl;
	it = alpha.begin();
	while(it != alpha.end()){
		printf("%c", *it);
		it++;
	}
	cout<<endl;
	it = other.begin();
	while(it != other.end()){
		printf("%c", *it);
		it++;
	}
	cout<<endl;
	return 0;
}

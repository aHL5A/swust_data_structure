/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <stdio.h>
using namespace std;

struct NODE{
	int data;
	int pos;
	bool operator < (NODE b){
		return this->data < b.data;
	};
};
int main()
{
	int n;
	vector<NODE>p;
	cin>>n;
	if(n==0)return 0;
	for(int i = 0; i < n; i++){
		NODE *node = new NODE;
		cin>>node->data;
		node->pos = i;
		p.push_back(*node);
	}
	sort(p.begin(), p.end());
	printf("Lmax=%d\r\n",(--p.end())->pos); //  \r ����д����Ȼ��ʾPE
	printf("Lmin=%d",p.begin()->pos);
	return 0;
}


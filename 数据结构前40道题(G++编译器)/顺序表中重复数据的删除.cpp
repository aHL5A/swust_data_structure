/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
#include <list>
using namespace std;
int main()
{
	int n, t, m;
	list<int>p;
	cin>>n;
	while(n--){
		cin>>t;
		p.push_back(t);
	}
	cin>>m;
	p.remove(m);
	if(p.empty()){
		cout<<-1;
		return 0;
	}
	list<int>::iterator it = p.begin();
	while(it != p.end()){
		cout<<*it<<" ";
		it++;
	}
	return 0;
}

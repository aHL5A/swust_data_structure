/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
#include <list>
using namespace std;
int main()
{
	int n, t, i, len;
	list<int>p;
	cin>>n;
	len = n;
	while(n--){
		cin>>t;
		p.push_back(t);
	}
	cin>>i;
	if(i < 2 || i > len){
		cout<<"error!";
		return 0;
	}
	list<int>::iterator pos = p.begin();
	for(int j = 1; j <= i-2; j++){
		pos++;
	}
	p.erase(pos);
	for(pos = p.begin(); pos != p.end(); pos++){
		cout<<*pos<<" ";
	}
	return 0;

}

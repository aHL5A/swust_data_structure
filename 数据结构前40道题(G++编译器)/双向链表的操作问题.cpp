/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int main()
{
	vector<int>p;
	int m, t;
	cin>>m;
	while(m--){
		cin>>t;
		p.push_back(t);
	}
	sort(p.begin(), p.end());
	vector<int>::iterator it = p.begin();
	while(it != p.end()){
		cout<<*it<<" ";
		it++;
	}
	return 0;
}

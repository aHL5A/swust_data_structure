/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;
struct node{
	int a;
	int x;
	bool operator < (node &B) const{
		return this->x < B.x;
	}
};

int main()
{
	vector<struct node>p;
	int n, m;
	char s[20];
	while(cin>>s){
		sscanf(s,"%d,%d", &n, &m);
		if(n==0 && m==0)break;
		node q;
		q.a = n;
		q.x = m;
		p.push_back(q);
	}
	vector<struct node>::iterator it = p.begin();
	while(cin>>s){
		bool flag = true;
		it = p.begin();
		sscanf(s,"%d,%d", &n, &m);
		node q;
		q.a = n;
		q.x = m;
		if(n==0 && m==0)break;
		while(it != p.end() && flag){
			if(m==it->x){
				it->a += n;
				flag = false;
			}
			it++;
		}
		if(flag)p.push_back(q);
	}
	sort(p.begin(), p.end());
	it = p.begin();
	while(it != p.end()){
		if(it->a != 0 && it != --p.end())cout<<it->a<<"x^"<<it->x<<"+";
		if(it->a != 0 && it == --p.end())cout<<it->a<<"x^"<<it->x;
		it++;
	}
	return 0;
}

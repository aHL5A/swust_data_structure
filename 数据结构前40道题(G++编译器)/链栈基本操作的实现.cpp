/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/16
 */

#include <iostream>
#include <stack>
using namespace std;
int main()
{
	stack<int>p;
	int n, t, m;
	cin>>n;
	while(n--){
		cin>>t;
		p.push(t);
	}
	cin>>m;
	while(m--){
		if(!p.empty())p.pop();
		else{
			cout<<-1;
			return 0;
		}
	}
	if(!p.empty())cout<<p.top();
	else cout<<-1;
	return 0;
}

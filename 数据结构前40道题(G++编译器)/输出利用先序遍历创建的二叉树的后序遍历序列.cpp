/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/15
 */

#include <iostream>
using namespace std;
int flag = 0;
typedef struct node{
	char data;
	node *L_child;
	node *R_child;
}Tree;

void Init(Tree *&T)
{
	char c;
	cin>>c;
	if(c=='#'&&flag==0){
		T = NULL;
		return;
	}
	if(c!='#'){
		flag=1;
		T = new Tree;
		T->data = c;
		Init(T->L_child);
		Init(T->R_child);
	}
	else T = NULL;
}

void display(Tree *&T)
{
	if(T==NULL)return;
	display(T->L_child);
	display(T->R_child);
	cout<<T->data;
}

int main()
{
	Tree *T;
	Init(T);
	display(T);
	return 0;
}
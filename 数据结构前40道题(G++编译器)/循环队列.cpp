/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include<iostream>
#include<string>
using namespace std;
int n;
typedef struct queue{
    int data[50];
    int head,tail;
}queue,*Queue;

void init(Queue queue){
    queue->head=queue->tail=0;
}
bool isEmpty(Queue queue){
    return queue->head==queue->tail;
}
bool isFull(Queue queue){
    return (queue->tail+1)%n==queue->head;
}
void in(Queue queue){
    int num;cin>>num;
    if(isFull(queue))return;
    queue->data[queue->tail++]=num;
    queue->tail=queue->tail%n;
}
int out(Queue queue,int &e){
    if(queue->head==queue->tail)return 0;
    e=queue->data[queue->head];
    queue->head=(queue->head+1)%n;
    return 1;
}

int main(){
    int t;
    queue queue;
    init(&queue);
    cin>>n>>t;
    string str;
    int num;
    int e=1;
    for(int i=0;i<t;i++){
        cin>>str;
        if(str=="in"){
            in(&queue);
        }
        else if(str=="out")num=out(&queue,e);
    }
    while(out(&queue,e)){
        cout<<e<<" ";
    }
    return 0;
}

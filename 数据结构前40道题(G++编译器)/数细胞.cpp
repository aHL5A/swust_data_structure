/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/14
 */

#include <iostream>
using namespace std;
int cell[100][100]={0};
int vis[100][100];
int dir[4][2]={{0,1},{1,0},{0,-1},{-1,0}};
int m, n, ans;
void dfs(int x, int y, int num)
{
	ans = num;
	for(int i = 0; i < 4; i++){
		int next_x = x + dir[i][0];
		int next_y = y + dir[i][1];
		if(next_x<1||next_x>m||next_y<1||next_y>n||cell[next_x][next_y]==0)
			continue;
		if(cell[next_x][next_y] != 0 && vis[next_x][next_y]==0){
			vis[next_x][next_y] = num;
			dfs(next_x, next_y, num);
		}
	}
}

int main()
{
	int k = 1;
	
	cin>>m>>n;
	for(int i = 1; i <= m; i++){
		for(int j = 1; j <= n; j++){
			cin>>cell[i][j];
		}
	}
	for(int i = 1; i <= m; i++){
		for(int j = 1; j <= n; j++){
			if(vis[i][j]==0 && cell[i][j]!=0){
				dfs(i, j, k);
				k++;
			}
		}
	}
	cout<<ans;
	return 0;
}

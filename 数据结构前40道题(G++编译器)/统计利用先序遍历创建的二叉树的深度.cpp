/*
 * Created by Dev c++ 5.11
 * Author: liyih
 * Date: 2018/03/15
 */

#include <iostream>
using namespace std;
typedef struct node{
	char data;
	node *L_child;
	node *R_child;
}Tree;

void Init(Tree *&T)
{
	char c;
	cin>>c;
	if(c!='#'){
		T = new Tree;
		T->data = c;
		Init(T->L_child);
		Init(T->R_child);
	}
	else T = NULL;
}

int Deepth(Tree *T)
{
	int suma = 0, sumb = 0;
	if(T==NULL)return 0;
	suma = Deepth(T->L_child);
	sumb = Deepth(T->R_child);
	if(suma>sumb) return suma+1;
    else return sumb+1;
}

int main()
{
	Tree *T;
	Init(T);
	cout<<(Deepth(T));
	return 0;
}


#include <iostream>
using namespace std;
typedef struct node{
	char data;
	node *L_child;
	node *R_child;
}Tree;

void Init(Tree *&T)
{
	char c;
	cin>>c;
	if(c!='#'){
		T = new Tree;
		T->data = c;
		Init(T->L_child);
		Init(T->R_child);
	}
	else T = NULL;
}

void count(Tree *&T, char k, int &ans)
{
	if(T != NULL){
		if(T->data == k){
			if(T->L_child){
				ans++;
			}
			if(T->R_child){
				ans++;
			}
			return;
		}
		count(T->L_child, k, ans);
		count(T->R_child, k, ans);
	}
}

int main()
{
	char k;
	int ans = 0;
	Tree *T;
	Init(T);
	cin>>k;
	count(T, k, ans);
	cout<<ans;
	return 0;
}

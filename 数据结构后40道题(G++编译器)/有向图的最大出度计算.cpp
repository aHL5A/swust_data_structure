#include <iostream>
#include <cstring>
using namespace std;
int main()
{
	int n, e, t1, t2, m = -1;
	cin>>n>>e;
	int *p = new int[n];
	memset(p, 0, sizeof(int)*n);
	for(int i = 0; i < e; i++){
		cin>>t1>>t2;
		p[t1]++;
		m = m > p[t1] ? m : p[t1];
	}
	cout<<m<<endl;
	for(int i = 0; i < n; i++){
		if(m == p[i])cout<<i;
	}
	delete[] p;
	return 0;
}

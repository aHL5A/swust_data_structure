#include <iostream>
using namespace std;
typedef struct node{
	char data;
	node *L_child;
	node *R_child;
}Tree;

void Init(Tree *&T)
{
	char c;
	cin>>c;
	if(c!='#'){
		T = new Tree;
		T->data = c;
		Init(T->L_child);
		Init(T->R_child);
	}
	else T = NULL;
}

void put(Tree *&T, char k)
{
	if(T != NULL){
		if(T->data == k){
			if(T->L_child != NULL)
				cout<<"L:"<<T->L_child->data<<",";
			else cout<<"L:#,";
			if(T->R_child != NULL)
				cout<<"R:"<<T->R_child->data;
			else cout<<"R:#";
		}
		put(T->L_child, k);
		put(T->R_child, k);
	}
}

int main()
{
	char k;
	Tree *T;
	Init(T);
	cin>>k;
	put(T, k);
	return 0;
}

#include <iostream>
using namespace std;
int cnt = 0, flag = 0;
struct Node{
	int data;
	Node *Lch;
	Node *Rch;
};


void initTree(Node *&T, Node *&p)
{
	if(p->data > T->data){
		if(T->Rch != NULL)
			initTree(T->Rch, p);
		else T->Rch = p;
	}
	else{
		if(T->Lch != NULL)
			initTree(T->Lch, p);
		else T->Lch = p;
	}
}

void search(Node *&T, int k)
{
	cnt++;
	if(T->data == k){
		flag = 1;
		return;
	}
	else if(k > T->data){
		if(T->Rch != NULL)search(T->Rch, k);
	}
	else{
		if(T->Lch != NULL)search(T->Lch, k);
	}
}

int main()
{
	int n, k;
	Node *Tree = new Node;
	Tree->Lch = Tree->Rch = NULL;
	cin>>n;
	cin>>Tree->data;
	for(int i = 1; i < 12; i++){
		Node *p = new Node;
		p->Lch = p->Rch = NULL;
		cin>>p->data;
		initTree(Tree, p);
	}
	cin>>k;
	search(Tree, k);
	cout<<(flag?cnt:-1);
	return 0;
}

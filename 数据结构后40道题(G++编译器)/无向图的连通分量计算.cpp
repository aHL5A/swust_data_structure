#include <iostream>
#include <queue>
#include <set>
using namespace std;
int ary[100][100]={0}, vis[100][100]={0};
void bfs(int n)
{
	queue<int>p;
	for(int i = 0, k = 1; i < n; i++, k++){
		p.push(i);
		while(!p.empty()){
			for(int j  = 0; j < n; j++){
				if(p.front() == j)continue;
				if(ary[p.front()][j] && !vis[p.front()][j]){
					vis[p.front()][j] = vis[j][p.front()] = k;
					p.push(j);
				}
			}
			p.pop();
		}
	}
}

int main()
{
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		for(int j = 0;j < n; j++){
			cin>>ary[i][j];
		}
	}
	bfs(n);
	set<int>ans;
	for(int i = 0; i < n; i++){
		for(int j = 0;j < n; j++){
			ans.insert(ary[i][j]);
		}
	}
	cout<<ans.size()-1;
	return 0;
}

#include <iostream>
using namespace std;
typedef struct node{
	char data;
	node *L_child;
	node *R_child;
}Tree;

void Init(Tree *&T)
{
	char c;
	cin>>c;
	if(c!='#'){
		T = new Tree;
		T->data = c;
		Init(T->L_child);
		Init(T->R_child);
	}
	else T = NULL;
}

Tree *put(Tree *&T, char k)
{
	if(T != NULL){
		if(T->L_child){
			if(T->L_child->data == k)return T;
		}
		if(T->R_child){
			if(T->R_child->data == k)return T;
		}
		put(T->L_child, k);
		put(T->R_child, k);
	}
	return NULL;
}

int main()
{
	char k;
	Tree *T;
	Init(T);
	cin>>k;
	Tree *ans = put(T, k);
	cout<<(ans!=NULL?ans->data:'#');
	return 0;
}

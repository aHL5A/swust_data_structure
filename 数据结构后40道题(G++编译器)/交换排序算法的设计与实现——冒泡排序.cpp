#include <iostream>
using namespace std;
int main()
{
	int n;
	cin>>n;
	int *p = new int[n];
	for(int i = 0; i < n; i++){
		cin>>p[i];
	}
	for(int j = 0, i = 1; j+1 < n; j++, i++){
		if(p[j] > p[i]){
			int t = p[j];
			p[j] = p[i];
			p[i] = t;
		}
	}
	for(int i = 0; i < n; i++){
		cout<<p[i]<<" ";
	}
	return 0;
}

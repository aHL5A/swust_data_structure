#include <iostream>
#include <string>
using namespace std;
string mid, last;
int root;
struct Node{
	char data;
	Node *Lch;
	Node *Rch;
};

void initTree(int l, int r, Node *&T)
{
	
	if(l>r ){
		T = NULL;
		return;
	}
	//cout<<l<<"  "<<r<<"   "<<root<<"--->"<<last[root]<<endl; //可输出构建的路径
	int i;
	T = new Node;
	T->data = last[root];
	for(i = l; i < r; i++){
		if(mid[i] == last[root])break;
	}
	root--;
	initTree(i+1, r, T->Rch);
	initTree(l, i-1, T->Lch);
}

void show(Node *&T)
{
	if(T == NULL)return;
	cout<<T->data;
	show(T->Lch);
	show(T->Rch);
}

int main()
{
	Node *Tree;
	cin>>mid>>last;
	root = last.length() - 1;
	initTree(0, last.length()-1, Tree);
	show(Tree);
	return 0;
}

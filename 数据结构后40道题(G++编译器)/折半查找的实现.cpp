#include <iostream>
using namespace std;
int cnt = 0, *p, pos = -1;
void search(int l, int r, int k)
{
	if(l > r)return;
	int mid = (l+r)/2;
	cnt++;
	if(p[mid] == k){
		pos = mid;
		return;
	}
	if(p[mid] >= k)search(l, mid-1, k);
	else search(mid+1, r, k);
}
int main()
{
	int n, k;
	cin>>n;
	p = new int[n];
	for(int i = 0; i < n; i++){
		cin>>p[i];
	}
	cin>>k;
	search(0, n-1, k);
	cout<<pos<<endl<<cnt;
	delete[] p;
	return 0;
}

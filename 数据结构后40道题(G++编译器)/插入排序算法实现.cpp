#include <iostream>
using namespace std;
int main()
{
	int n;
	cin>>n;
	int *p = new int[n];
	for(int i = 0; i < n; i++){
		cin>>p[i];
	}
	for(int i = 1; i < 2; i++){ //当写成 i < n 时就是完整的插入排序
		int j = i, t = p[i];
		while(j > 0 && t < p[j-1]){
			p[j] = p[j-1];
			j--;
		}
		p[j] = t;
	}
	for(int i = 0; i < n; i++){
		cout<<p[i]<<" ";
	}
	return 0;
}

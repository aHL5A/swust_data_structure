#include <iostream>
#include <vector>
#include <queue>
#include <cstring>
using namespace std;
vector<int>p[100];
int vis[100] = {0};
void bfs(int n)
{
	int cnt = 0, t;
	for(int i = 0; i < n; i++){
		memset(vis, 0, sizeof(int)*n);
		if(!p[i].empty()){
			vis[i] = 1;
			queue<int>loop;
			loop.push(i);
			while(!loop.empty()){
				int k = loop.front();
				for(int j = 0; j < p[k].size(); j++){
					if(!vis[p[k][j]]){
						vis[p[k][j]] = 1;
						loop.push(p[k][j]);
					}
				}
				loop.pop();
			}
		}
		else{
			cout<<"no";
			return;
		}
		t = 0;
		for(int i = 0; i < n; i++){
			t += vis[i];
		}
		if(t == n)cnt++;
	}
	cout<<(cnt==n?"yes":"no");
}

int main()
{
	int n, e, from, to;
	cin>>n>>e;
	for(int i = 0; i < e; i++){
		cin>>from>>to;
		p[from].push_back(to);
	}
	bfs(n);
	return 0;
}

#include <iostream>
using namespace std;
void down(int k, int *&p, int n)
{
	int i = k;
	while(k * 2 <= n){  //确保有左孩子结点
		if(p[2*k] < p[k]){
			i = 2*k;
		}
		if(2*k+1 <= n){ //如果右孩子结点
			if(p[2*k+1] < p[i]){
				i = 2*k+1;
			}
		}
		if(i != k){
			int t = p[i];
			p[i] = p[k];
			p[k] = t;
			k = i;  //继续向下调整
		}
	}
}
void creatheap(int *&p, int n)  //创建一个小堆
{
	for(int i = n/2; i >= 1; i--){
		down(i, p, n);
	}
}
int main()
{
	int n;
	cin>>n;
	int *p = new int[n+1];
	for(int i = 1; i <= n; i++){
		cin>>p[i];
	}
	creatheap(p, n);
	for(int i = 1; i <= n; i++){
		cout<<p[i]<<" ";
	}
	return 0;
}

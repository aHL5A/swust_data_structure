#include <iostream>
#include <cstring>
using namespace std;
int main()
{
	int n, t, k, ans = 0;
	cin>>n>>k;
	int *p = new int[n];
	memset(p, 0, sizeof(int)*n);
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			cin>>t;
			p[i] += t;
		}
		if(p[i] == k)ans++;
	}
	cout<<ans<<endl;
	for(int i = 0; i < n; i++){
		if(k == p[i])cout<<i;
	}
	delete[] p;
 	return 0;
}

#include <iostream>
#include <queue>
using namespace std;
int flag = 0;
typedef struct node{
	char data;
	node *L_child;
	node *R_child;
}Tree;

void Init(Tree *&T)
{
	char c;
	cin>>c;
	if(c=='#'&&flag==0){
		T = NULL;
		return;
	}
	if(c!='#'){
		flag=1;
		T = new Tree;
		T->data = c;
		Init(T->L_child);
		Init(T->R_child);
	}
	else T = NULL;
}

void Out(Tree *&T)
{
	queue<Tree *>p;
	p.push(T);
	while(!p.empty()){
		cout<<p.front()->data;
		if(p.front()->L_child)p.push(p.front()->L_child);
		if(p.front()->R_child)p.push(p.front()->R_child);
		p.pop();
	}
}

int main()
{
	Tree *T;
	Init(T);
	Out(T);
	return 0;
}

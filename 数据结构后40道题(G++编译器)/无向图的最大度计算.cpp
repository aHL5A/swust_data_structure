#include <iostream>
#include <cstring>
using namespace std;
int main()
{
	int n, t, m = -1;
	cin>>n;
	int *p = new int[n];
	memset(p, 0, sizeof(int)*n);
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			cin>>t;
			p[i] += t;
		}
		m = m > p[i] ? m : p[i];
	}
	cout<<m<<endl;
	for(int i = 0; i < n; i++){
		if(m == p[i])cout<<i;
	}
	delete[] p;
 	return 0;
}

#include <iostream>
#include <string>
#include <cctype>
#include <cstring>
using namespace std;
int main()
{
	int n;
	string s;
	cin>>n;
	cin.get();
	for(int i = 0; i < n; i++){
		int *p = new int[n];
		//C11中可直接写int *p = new int[n]{0}进行初始化，不需要下一句
		memset(p, 0, sizeof(int)*n);
		getline(cin, s);
		for(int j = 0; j < s.length(); j++){
			if(isdigit(s[j])){
				int t = int(s[j]-'0');
				p[t] = 1;
			}
		}
		for(int k = 0; k < n; k++){
			cout<<p[k];
		}
		cout<<endl;
	}
	return 0;
	
}

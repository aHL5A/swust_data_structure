#include <iostream>
#include <cstring>
using namespace std;
int main()
{
	int n, e, t1, t2;
	cin>>n>>e;
	int *p = new int[n];
	memset(p, 0, sizeof(int)*n);
	for(int i = 0; i < e; i++){
		cin>>t1>>t2;
		p[t1]++;
	}
	for(int i = 0; i < n; i++){
		cout<<p[i]<<endl;
	}
	return 0;
}

#include <iostream>
#include <string>
using namespace std;
string mid, first;
int root;
struct Node{
	char data;
	Node *Lch;
	Node *Rch;
};

void initTree(int l, int r, Node *&T)
{

	if(l>r ){
		T = NULL;
		return;
	}
	//cout<<l<<"  "<<r<<"   "<<root<<"--->"<<first[root]<<endl; //可输出构建的路径
	int i;
	T = new Node;
	T->data = first[root];
	for(i = l; i < r; i++){
		if(mid[i] == first[root])break;
	}
	root++;
	initTree(l, i-1, T->Lch);
	initTree(i+1, r, T->Rch);
}

void show(Node *&T)
{
	if(T == NULL)return;
	show(T->Lch);
	show(T->Rch);
	cout<<T->data;
}

int main()
{
	Node *Tree;
	cin>>mid>>first;
	root = 0;
	initTree(0, first.length()-1, Tree);
	show(Tree);
	return 0;
}
